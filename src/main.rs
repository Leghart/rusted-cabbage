pub mod utils;
use utils::logging;

fn main() {
    logging::setup_logger();
    log::info!("Hello world!");
    log::warn!("What is going on?")
}
