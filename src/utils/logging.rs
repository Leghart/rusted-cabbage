use chrono::Local;
use env_logger::{fmt::Color, Builder, Env};
use log::Level;
use std::io::Write;

#[inline(always)]
fn get_color_for_level(level: &Level) -> Color {
    match level {
        Level::Debug => Color::Blue,
        Level::Info => Color::White,
        Level::Warn => Color::Yellow,
        _ => Color::Red,
    }
}

pub fn setup_logger() {
    Builder::from_env(Env::default().default_filter_or("info"))
        .format(
            |buf: &mut env_logger::fmt::Formatter, record: &log::Record<'_>| {
                let mut level_style: env_logger::fmt::Style = buf.style();
                let current_level: Level = record.level();

                level_style.set_color(get_color_for_level(&current_level));
                level_style.set_bold(true);

                writeln!(
                    buf,
                    "{} [{}] {:?} - {}",
                    Local::now().format("%H:%M:%S:%m"),
                    level_style.value(&current_level),
                    std::thread::current().id(),
                    record.args(),
                )
            },
        )
        .init();
}
